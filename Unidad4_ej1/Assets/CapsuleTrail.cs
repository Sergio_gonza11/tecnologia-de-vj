﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleTrail : MonoBehaviour {

	[Range(1,50)]
	public float var1 = 5;
	[Range(1,50)]
	public float var2=2;
	// Update is called once per frame
	void Update () {
		GetComponent<TrailRenderer> ().time = Random.Range (1, 100);
		GetComponent<TrailRenderer> ().startWidth = var1;
		GetComponent<TrailRenderer> ().endWidth = var2;

		
	}
}
