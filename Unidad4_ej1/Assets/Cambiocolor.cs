﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cambiocolor : MonoBehaviour {

	public Renderer mirenderer;
	public Color color1;
	public bool modo;
	 
	void Start () {
		mirenderer=GetComponent<Renderer>();


	}
	
	// Update is called once per frame
	void Update () {
		if (modo == true) {
			if (Input.GetButton ("Color")) {
			
				mirenderer.material.SetColor ("_Color", Color.red);

			} else
				mirenderer.material.SetColor ("_Color", Color.white);
		} else {
			
			if ((modo == false) && (Input.GetButton ("Color"))) {
				mirenderer.material.color = Random.ColorHSV (0f, 1f, 1f, 1f, 0.5f, 1f);
			} else
				mirenderer.material.SetColor ("_Color", Color.white);
		}
		
		
		
			
		
		
	}
}
