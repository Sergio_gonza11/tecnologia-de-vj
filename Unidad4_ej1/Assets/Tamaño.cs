﻿using UnityEngine;
public class Tamaño : MonoBehaviour
{
    //VELOCIDAD DE LA CAPSULA
    public float scaleUnits = 5f;
    void start()
    {
        transform.localScale=(new Vector3(transform.localScale.x+scaleUnits * Time.deltaTime, 1, 1));
        Debug.Log("Moving From Start Event");
    }
    void Update()
    {
        transform.localScale=(new Vector3(transform.localScale.x + scaleUnits * Time.deltaTime, 1, 1));
        Debug.Log("Moving From Update Event");
    }
}
