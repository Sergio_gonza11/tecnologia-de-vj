﻿using UnityEngine;
public class Movimiento2 : MonoBehaviour
{
    //VELOCIDAD DE LA CAPSULA
    public float rotationDegrees = 5f;
    void start()
    {
        transform.Rotate(new Vector3(rotationDegrees * Time.deltaTime, 0, 0));
        Debug.Log("Moving From Start Event");
    }
    void Update()
    {
        transform.Rotate(new Vector3(0, 0, -rotationDegrees * Time.deltaTime));
        Debug.Log("Moving From Update Event");
    }
}
