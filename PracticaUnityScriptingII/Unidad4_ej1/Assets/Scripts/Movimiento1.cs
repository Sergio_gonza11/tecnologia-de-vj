﻿using UnityEngine;
public class Movimiento1 : MonoBehaviour
{
    //VELOCIDAD DE LA CAPSULA
    public float speed = 5f;
    void start()
    {
        transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
        Debug.Log("Moving From Start Event");
    }
    void Update()
    {
        transform.Translate(new Vector3(0, 0, -speed * Time.deltaTime));
        Debug.Log("Moving From Update Event");
    }
}
