﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {
	public GameObject objectToSpawn;
	public float secondsCounter = 0;
	public float secondsToCount;
	private Renderer mirenderer;
	public Color color1;
	void Start(){
		mirenderer = GetComponent<Renderer> ();
		secondsCounter = 0;
	}
		void Update ()
	{
		
		 secondsCounter+= Time.deltaTime;
		 secondsToCount=Random.Range(0.5f,3.0f);

		if (secondsCounter>=secondsToCount) {
					
			secondsCounter= 0;


					GameObject objAux = Instantiate (objectToSpawn, transform.position, Quaternion.identity) as GameObject;
					float scale = Random.Range (1, 10);
					objAux.transform.localScale = new Vector3 (scale, scale, scale);
					
				
					Rigidbody rb = objAux.GetComponent<Rigidbody> ();
					rb.drag = Random.Range (0f, 0.1f);
					rb.mass = Random.Range (1, 25);
					rb.AddForce (new Vector3( Random.Range (10, 150), Random.Range (10, 150),Random.Range (10, 150)),ForceMode.Impulse);
					secondsToCount=Random.Range(0.5f,3.0f);
					mirenderer = objAux.GetComponent<Renderer> ();
					mirenderer.material.SetColor ("_Color",color1);
					




						}
					


							
	}
}


	