﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class orbita : MonoBehaviour {
	public GameObject cube;
	public Transform center;
	public Vector3 axis=Vector3.up;
	public Vector3 desiredPosition;
	public float radius;
	public float radiusSpeed=0.5f;
	public float Vrotacion=80.0f;
	// Use this for initialization
	void Start () {
		center =cube.transform;
		transform.position = (transform.position - center.position).normalized * radius + center.position;
	}

	// Update is called once per frame
	void Update ()
	{
		transform.RotateAround (center.position, axis, Vrotacion * Time.deltaTime);
		desiredPosition = (transform.position - center.position).normalized * radius + center.position;
		transform.position = Vector3.MoveTowards (transform.position, desiredPosition, Time.deltaTime * radiusSpeed);
	}
}

	

