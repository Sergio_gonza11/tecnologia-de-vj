﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereMovement : MonoBehaviour {

	public GameObject capsule;
	void Start () {
		if (capsule == null) {
			Debug.LogError ("No capsule GameObjet");
			enabled = false;
			return;


		}
		transform.position = capsule.transform.position - Vector3.forward * 10f;
	}
	// Update is called once per frame
	void Update () {
		transform.position = capsule.transform.position - Vector3.forward * 10f;


	}
}

